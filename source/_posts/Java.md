# Java学习扫盲

## 前言

​	笔者已经学习过了c++基础，在此基础上学习java，查漏补缺。

## 第一个源程序

- 一个.java文件一般**很少定义多个class类**，一般只有一个class

- 一个.java文件**只能有一个**public class 类，多个非public class

- 类名称以大写开头

```java
package HelloWorldPrin;


public class Hello {
    public static void main(String[] args){
        System.out.println("hello");
    }
}
```



## 面向对象

### 面向对象与面向过程

- 面向过程：
  - 代表语言：c，c++
  - 思想：面对一个问题的解决方案，没有模块化的设计
- 面向对象：
  - 代表语言：java
  - 思想：模块化设计，注重重用

### 引用传递

- 类似C++的**浅拷贝**，具体看例子：


```java
   class person {
    person(){

    }
    int age;
    String name;
}

public class Cite {
    public static void main(String[] args) {
        person per1=new person();
        per1.name="张三";
        per1.age=20;

    }
}

```

- 以下两个代码块都使得per1的年龄改变成80：


1. 函数调用形式：

```java
change(per1);
public static void  change(person tmp){
    tmp.age=80;
}
```

2. 引用形式：


```java
    person per2=per1;
    per2.age=80;
```

- 而简单变量进行的是**深拷贝**

```java
int x=80;
int y=x;
y=10;
System.out.println(x);//80
```

### 引用传递与垃圾分析

``

```java
person per1=new person();
person per2=new person();
per1.name="张三";
per1.age=20;
per2.name="李四";
per2.age=80;
per1=per2;//per1的原内存泄露，per1与per2的地址指向相同；泄露的内存会被系统回收
```



### 引用传递实例

### 匿名对象

``

- 构造函数：

```
public person(int age,String name){
    this.age=age;
    this.name=name;
}
```

- 调用代码：

  `new person(10,"王五").changeAge(10);`



### static 关键字

1. 修饰类变量

- ​	和c++对标，**static修饰的成员变量属于类**，有节省内存空间，便于修改的作用。

- ​	具体应用：中华民国，注册1w人

``

```java
import java.util.Scanner;

class Person{
    String country="中华民国";
    String name;
    int age;
    Person( String name, int age){
        this.name = name;
        this.age = age;
    }
}

public class static_use{
    public static void main(String[] args) {
        Person[] people=new Person[10000];
        for(int i=0; i<10000;i++){
            Scanner input=new Scanner(System.in);
            String name=input.nextLine();
            int age=input.nextInt();
            people[i]=new Person(name, age);
        }
    }
}
```

​	此时新中国成立，中华民国的所有人的国际都需要更改，而遍历更高效率很低，若定义**country**时加上static关键字即可解决这个问题：

`static String country="中华民国";`



2. 修饰类方法
   - static方法只能访问静态对象和静态方法
   - 非static方法可以访问静态对象和静态方法




3. static实例：记录总人数

``

```java
import java.util.Scanner;

class Person{
    static String country="中华民国";
    String name;
    int age;
    private static int count=0;
    Person( String name, int age){
        //避免没有名字的情况
        if(name==" "){
            this.name = name;
        }else{
            this.name ="NO_NAME-"+count;
        }
        this.age = age;
        System.out.println("new book!! now new book count is: "+count);
        System.out.println("bookInfo: name:"+this.name+" age:"+this.age);
        count++;
    }
}

public class static_use{
    public static void main(String[] args) {
        Person[] people=new Person[10000];
        for(int i=0; i<10000;i++){
            Scanner input=new Scanner(System.in);
            String name=input.nextLine();
            int age=input.nextInt();
            people[i]=new Person(name, age);
        }
    }
}
```



### 代码块

- ​	定义：使用{}定义的结构

- ​	分类：

  1.  普通代码块 

      自定义的界限。

     ``

     ```java
     public static void main(String[] args) {
        	{int x=10;}
        	int x=100;
         }
     ```

  2. 构造代码块：定义在类中的块，其优先于构造函数执行：

     ```java
      class demo {
         demo(){
             System.out.println("构造方法执行");
     
         }
         //构造块
         {
             System.out.println("构造块执行");
         }
     }
     public class construct_block{
         public static void main(String[] args) {
             new demo();
             new demo();
             new demo();
         }
     }
     ```

     输出结果：![image-20211109211931195](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20211109211931195.png)

     

  3. 静态代码块：使用static关键字定义的代码块

     - 静态代码块优先于构造块执行

     - 在非主类中定义静态代码块：

       - 用于**类中静态属性的初始化**

         ``

         ```java
          class demo {
              private static String demotype;
             //静态构造块
             {
                 //如果demo被创建多次，则只执行一次demotype的初始化
                 demotype="learnning";
                 System.out.println("静态构造块执行");
             }
         }
         public class code_block{
             public static void main(String[] args) {
                 new demo();
             }
         }
         ```

     - 在主类中定义静态代码块：

       - 静态代码块优先于主方法先执行

         ```java
         public class code_block{
              static{
                  System.out.println("......初始化......");
                  //...可以在主程序执行前做一些初始化工作
              }
             public static void main(String[] args) {
                 System.out.println("主方法执行");
             }
         }
         ```

     

## Java相关技术

### maven

#### 	介绍

 	Apache 下的一个纯 Java 开发的开源项目。基于项目对象模型（缩写：POM）概念，Maven利用一个中央信息片断能管理一个项目的构建、报告和文档等步骤。**Maven 是一个项目管理工具**，可以对 Java 项目进行构建、依赖管理。

​	Maven 也可被用于构建和管理各种项目，例如 C#，Ruby，Scala 和其他语言编写的项目。Maven 曾是 Jakarta 项目的子项目，现为由 Apache 软件基金会主持的独立 Apache 项目。

​	

### Spring全家桶

#### Spring Framework

- ##### core

​	-IoC，AOP

- ##### Data Access

  -Transactions,Spring MyBatis

- ##### Web Servlet

  -Spring MVC

- ##### Integration

#### Spring Boot

#### Spring Cloud(暂时用不上)

#### Spring Cloud Data Flow(暂时用不上)

### Spring Boot demo实例

​	Spring Boot 是为了**简化 Spring** 应用的搭建和开发过程，Pivotal 团队在 Spring 基础上提供了一套全新的开源的**框架**

​	创建Spring Boot的视频教程：https://www.bilibili.com/video/BV15b4y1a7yG?p=3

## Annotation

### 简介

- Annotation(注解)是从JDK1.5之后提出的一个新的开发技术结构，利用Annotation可以一些结构化的定义。Annotation是以一种**注解的形式实现的程序开发**。
- 程序开发结构的历史过程：
  1. ​	在程序定义的时候将所有可能使用到的资源全部定义在程序代码之中：
     - 如果此时服务器的相关的**地址发生了改变**，那么对于程序而言就需要进行源代码这样的做法明显是不方便的：
  2. 引入配置文件，在配置文件之中定义全部要使用的服务器资源
     - 在配置项不多的情况下，此类配置非常好用，并且十分的简单，但是如果这个
       时候所有项目都采用此结构么就可能出现一种可怕的场景：配置文件特别多。
     - 所有的操作都需要通过配置文件完成，这样对于开发的难度提升了；
  3. 将配置信息重新写回到程序里面，利用一些**特殊的标记**与程序代码进行分离，**即注解**
     - 全部用注解开发难度太大，所以现在是注解+配置文件同时使用。

### 注解举例：@override

- 没写子类覆盖某父类方法时，没写 **extends**，子类的connect()无法被调用，但不报错。
- 子类connect写成了connection()，不报错，但程序也不能按照预期执行

![image-20220419214410721](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192144842.png)

![image-20220419214644766](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192146839.png)

- 如果在该方法前加上@override，在上述两种情况中会报错，以提示开发者。



## 包的定义及使用

- 包(package)：就是**目录/文件夹**，使用点分隔符定义多个层级
- 命名规则
  - 1、统一使用小写
  - 2、点分隔符之间有且仅有一个自然语义的单词
  - 3、包名使用单数形式
  - 4、一般建议使用项目**域名倒序**作为包名前缀
    如：org.apache.dubbo、org.apache.ibatis等
- 创建包
  在类中使用**package关键词**去创建一个包，如：package org.apache.dubbo;
- 导入包
  使用别人的类或与当前代码不在同一个包的类需要**通过import去导入**
  如：import org.apache.ibatis.Car;

## 内部类

### 内部类概述

- 内部类	：就是在一个类中定义一个类。举例：在一个类的内部定义一个类B,类B就被称为内部类
- 内部类格式

![image-20220419220306125](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192203203.png)

内部类访问特点

- 内部类可以直接访问外部类的成员，**包括私有**
- 外部类要**访问内部类的成员**，**必须创建对象**

### 成员内部类

按照内部类在**类中定义的位置不同**，可以分为如下两种形式：

- 在类的成员位置：成员内部类
- 在**类的方法里面**定义的类：局部内部类

成员内部类，外界如何创建对象使用呢？

- 格式：外部类名.内部类名.对象名=外部类对象.内部类对象

- 示例：

  主函数

  ```java
  package com;
  public class demo {
      public static void main(String[] args){
          outer o =new outer();
          o.method();
          System.out.println("hello");
      }
  }
  
  ```

  outer类

  ```java
  package com;
  public class outer {
          private int num = 10;
  
   private class Inner {
          public void show() {
              System.out.println(num);
          }
      }
     public void method(){
          Inner i = new Inner();//访问Inner必须创建对象
          i.show();
      }
  
  }
  
  ```

  

### 局部内部类

​	局部内部类是在**方法中定义的类**，所以外界是无法直接使用，需要在方法内部创建对象并使用
该**类可以直接访问外部类的成员**，也可以**访问方法内的同部变量**

- 示例：

  outer类

```
package com;
public class outer {
        private int num = 10;

        public void method() {
            int num2 = 20;
            class Inner {
                public void show() {
                    System.out.println(num);//访问外部类的成员
                    System.out.println(num2);//访问方法内的同部变量
                    Inner i = new Inner();
                    i.show();
                }
            }
        }

}

```

​		主函数	

```
package com;
public class demo {
    public static void main(String[] args){
        outer o =new outer();
        o.method();
        System.out.println("hello");
    }
}
```

### 匿名内部类

- 前提：存在一个类或者接口，这里的类可以是具体类也可以是抽象类

- 本质：是一个**继承了该类或者实现了该接口的子类**匿名对象

- 格式

  ![image-20220420003117606](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204200031789.png)

- 示例

  outer类

  ```java
  package com;
  public class outer {
  
      public void method(){
  //        new Inter(){//本质是对象，没有名字的对象
  //            @Override
  //            public void show()
  //            {
  //                System.out.println("匿名内部类");
  //            }
  //        };
  
  
  //        //匿名对象调用方法，上面注释的代码无法执行
  //        new Inter(){//本质是对象，没有名字的对象
  //            @Override
  //            public void show()
  //            {
  //                System.out.println("匿名内部类");
  //            }
  //        }.show();
  
          //赋值给接口,然后多次调用，上述的调用方式不适合多次
         Inter i= new Inter(){
              @Override
              public void show()
              {
                  System.out.println("匿名内部类");
              }
          };
         i.show();
         i.show();
         i.show();
      }
  
  }
  
  ```

  主函数

  ```java
  package com;
  public class demo {
      public static void main(String[] args){
          outer o =new outer();
          o.method();
  
      }
  }
  
  ```



## 函数式接口

- 函数式接口：**有目仅有一个抽象方法的接口**。
- Java中的函数式编程体现就是Lambda表达式，所以函数式接口就是可以适用于Lambda使用的接口，只有确保接口中有且仅有一个抽象方法，Java中的Lambda才能顺利地进行准导。
- 如何检测一个接口是不是函数式接口呢？
  - @Functionallnterface
  - 放在接口定义的上方：如果接口是函数式接口，编泽通过：如果不是，编译失败
- 注意
- 我们自己定义函数式接口的时候，@Functionallnterface是可选的，就算我不写这个注解，只要保证满足函数式接口定义的条件，也照样是函数式接口。但是，建议加上该注解。
