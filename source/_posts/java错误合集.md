# java 错误合集

## idea问题

### java模块无法运行main函数

- **错误描述**
  - 新建的java模块无法运行main函数，**但是新建的maven项目可以运行main函数**,暂无解决方案，暂时都用maven创建项目即可。

![image-20220419234950907](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192349031.png)

### 不支持发行版本xx

- **错误信息**：

  - Error:java: 错误: 不支持发行版本 14

  - Error:java: 不再支持源选项 5。请使用 6 或更高版本。

  - Error:java: 不再支持源选项 5。

- **解决方法**：将以下位置的java版本弄成一致。

  - 第一处：maven项目位置(**使用maven时需要改**)：

    - 打开：maven安装目录/conf/**settings.xml**

    - ![image-20220419232651653](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192326931.png)

    - 在文件中搜索：maven.compiler.source

    - ![image-20220419232944142](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192329547.png)

    - 将定位到**properties**位置中**maven.compiler.source**等内容，将其中的**数字**改成已安装的java版本，这里选择**14**，**后续的版本选择也是14**。

    - ```
          <profile>
            <id>development</id>
            <activation>
              <jdk>14</jdk>//这里也可以改成14，易于理解
              <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
              <maven.compiler.source>14</maven.compiler.source>
              <maven.compiler.target>14</maven.compiler.target>
              <maven.compiler.compilerVersion>14</maven.compiler.compilerVersion>
            </properties>
          </profile>
      ```

  - 第二处：项目结构：

    - 点击文件->项目结构
    - ![image-20220419233206080](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192332193.png)
      - 项目结构->项目 的修改项：
      - ![image-20220419233316910](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192333068.png)
      - 项目结构->模块 的修改项：
      - ![image-20220419233445773](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192334996.png)
      - ![image-20220419233519906](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192335131.png)

  - 第三处：java编译器

    - 文件->设置
    - ![image-20220419234259330](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192342495.png)
    - 设置->构建、执行、部署->编译器->Java编译器
    - ![image-20220419234357232](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204192343448.png)
    
    - 最好把默认配置也修改了：**文件->其他设置->新项目的 设置...**
    - ![image-20220420010015081](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204200100182.png)
    - ![image-20220420005939429](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204200059732.png)
    
    

- 修改完后，即可成功运行。
