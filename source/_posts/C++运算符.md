# C++运算符
## 算数运算符
一共有7种，分别是：+，-，*,/,%,++,--

```
int a=10;
int b=20;
int c=0;

c=a+b;//30
c=a-b;//-10
c=a*b;//200
c=a/b;//0
c=a%b;//10
c++;//1
c--;//-1
```
## 关系运算符
一共6种,分别是：==,!=,>,<,>=,<=
```
bool flag;
int a=10,b=20;
flag=(a==b);//false
flag=(a!=b);//true
flag=(a>b);//false
flag=(a<b);//true
flag=(a>=b);//false
flag=(a<=b);//true
```
## 逻辑运算符
一共有3种，分别是：&&,||,!
```
int a=0,b=1;
bool flag;

flag=a&&b;//false
flag=a||b;//true
flag=!a;//true
```

## 位运算符
一共有3种，分别是&(与),|(或),^(异或)
![真值表](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/c++运算符.md/306645216235594.png)
假设如果 A = 60，且 B = 13，现在以二进制格式表示，它们如下所示：

A = 0011 1100

B = 0000 1101

-------位运算----------

A&B = 0000 1100

A|B = 0011 1101

A^B = 0011 0001

~A  = 1100 0011

## 移位运算符
共两种：>>右移,<<左移。
* 左移运算符(<<)在右侧插入值为0的二进制位；
* 右移运算符(>>)的行为依赖于其左侧运算对象的类型，如果运算对象是无符号类型，则在左侧插入值为0的二进制位；如果运算对象是有符号类型，则在左侧插入**符号位的副本**或者**值为0**的二进制位(正数肯定插入0，负数视情况而定)

假设int a=11,a的二进制表示为:1010，那么：
   
    a>>2,a右移两位,001011--->000010
    a<<2,a左移两位,001011--->101100
    
 
## 赋值运算符
![](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/c++运算符.md/440135516234411.png)

## 常用技巧
* x&1判断奇数偶数，若x&1为真，则x是奇数，否则是偶数。
* x&(x-1)可以消除x最右边的1，如8&7:
    8的二进制表示:1000
    7的二进制表示:0111
    8&7结果为:0000,8最右边的1被消除了。
    可用到的题目:
    * [191.位1的个数](https://leetcode-cn.com/problems/number-of-1-bits/)
    * [201.数字范围按位与](https://leetcode-cn.com/problems/bitwise-and-of-numbers-range/)
    * [231.2的幂](https://leetcode-cn.com/problems/power-of-two/)
  
  

## 数学运算技巧
### 在不使用64位整数的情况下，判断32位整数是否会溢出
(题目来源:[整数翻转](https://leetcode-cn.com/problems/reverse-integer/))
由于32位整数的范围是:[负2的31次方，2的三十一次方减一]，即[-2147483648,2147483647],、假设待判断的数为a,a接下来要执行的操作是:a=a*10+num,-9<=num<=9,且num的符号和a相同:
* 对于正数a:
    * 若a<214748364,则a的最大值为:2147483630,a*10+num的最大值为:2147483639，此时a在执行下一步操作后不会溢出
    * 若a=214748364,则a*10=2147483640，若num>7，此时a在执行下一步操作后会溢出，否则不会溢出
    * 若a>214748364,则a最小为2147483650,此时a在执行下一步操作会溢出
    * 所以a上溢的条件为：(a>214748364)||(a==214748364&&num>7)
* 对于负数a:
    * 若a<-214748364,则a的最大值为:-214748365，a*10=-2147483650,由num的符号与a相同,a在执行下一步操作后会溢出
    * 若a=-214748364,a*10=-2147483640，当num<-8时,a在执行下一步操作后会溢出
    * 若a<-214748364,则a的最小值为:-2147483630，此时a在执行下一步操作后不会溢出
    * 所以a下溢的条件为:(a<-214748364)||(a=214748364&&num<-8)
综上,a溢出的条件为:
(a>214748364)||(a==214748364&&num>7)||(a<-214748364)||(a=214748364&&num<-8)