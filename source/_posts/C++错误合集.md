# C++错误合集

## 迭代器删除时的正确处理方式

- 错误代码：导致迭代器失效，调试会报错。

```
            for (auto it = map.begin(); it != map.end();it++)
                if (it->second < n)map.erase(it);
               
```

```
                unordered_map<char, int>map;
				unordered_map<char, int>target_map;
				//剔除不满足条件的字符：
                for (auto it = map.begin(); it != map.end();it++)
                {
                    char x = it->first;
                    char num = target_map[x];
                    if (target_map.find(x) != target_map.end()&&num>=n) {
                        continue;
                       
                    }
                    map.erase(it);
                }
            }
```



- 正确代码：删除for循环中迭代器it的自增，在处理时加上。

```c++
        for (auto it = map.begin(); it != map.end();)
            if (it->second < n)map.erase(it++);
            else ++it;
```

```c++
               unordered_map<char, int>map;
				unordered_map<char, int>target_map;
				//剔除不满足条件的字符：
                for (auto it = map.begin(); it != map.end();)
                {
                    char x = it->first;
                    char num = target_map[x];
                    if (target_map.find(x) != target_map.end()&&num>=n) {
                        continue;
                        ++it;
                    }
                    map.erase(it++);
                }
            }
```

