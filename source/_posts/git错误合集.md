# git错误合集
## 换行符
### 错误详情
在上传html文件时，出现以下警告：

    warning: LF will be replaced by CRLF in  vue_base/01初始vue/初识vue.html.
    The file will have its original line endings in your working directory
### 错误原因
这是因为文件中换行符的差别导致的。这个提示的意思是说：会把windows格式（CRLF（也就是回车换行））转换成Unix格式（LF），这些是转换文件格式的警告，不影响使用。
### 解决方法
```

git rm -r --cached . //先将所有文件将暂存区移除
git config core.autocrlf false //取消自动转换文件格式
git add . //重新添加文件

```
### 来源
https://blog.csdn.net/qq_37521610/article/details/88327286

## 找不到远程仓库
### 错误详情
    在git push 时出现：
```
Please make sure you have the correct access rights
and the repository exists.

```
### 错误原因
    首次上传代码至gitgee没有配置ssh key。
### 解决方法
根据官方教程配置即可：https://gitee.com/profile/sshkeys



## 如何在git状态下重命名文件？

使用**git mv**命令，如：以下命令，将文件**readme.md** 更名为 hello.md。

```
git mv readme.md hello.md
```

