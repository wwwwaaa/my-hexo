# 雏鹰计划-C&&C++

## 编程工具演练(C语言)

​	git学习链接：https://learngitbranching.js.org/?locale=zh_CN

### 第一章 版本控制简介

#### 版本控制工具简介 

##### 通过学习本小节，你将掌握如下知识点：

1. 了解版本控制的发展过程与相关工具的演变有基础的了解

2. 了解 Git 的产生过程

3. 清楚 Git 是一种什么类型的版本控制工具

在进入本章学习前，助教小姐姐有两个问题要问你：

1. SVN 和 Git 的相同点是什么？

2. diff 工具产生的差异结果包含哪些内容？

##### 关于本章

###### 章节背景

版本控制是一种**记录一个或若干文件内容变化**，以便将来查阅特定版本修订情况的系统，方便查看更改历史，备份以及恢复以前的版本，保证多人的协作不出问题。在学习Git之前，我们有必要对版本控制以及相关工具的发展进行简单的说明。

###### 章节内容

* 原始的版本控制
* 版本控制的起源：diff与patch
* RCS最早期的本地版本控制工具
* CVS,SVN集中式版本控制工具
* Git:Linux的第二个伟大作品
* 结语：什么是Git

###### 原始的版本控制

版本控制工具的黑暗时代：

* 最原始的版本控制是纯手工的版本控制：修改文件，保存文件副本；
* 保存副本命名随意一版本难辨新旧，不能辨别每一版的修改内容。
* 两个例子：
  ![毕业论文](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/579713020238798.png)
  ![游戏地图](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/157263120226665.png)

###### 版本控制的起源：diff与patch

在最初的版本控制软件出现之前，其实已经有了比较好用的源码比较与打补丁的工具：diff与patch

* Linus Torvalds(Linux,之父)也对这两个工具偏爱有加.
* 在1991-2002年之间，即使CVS出现以后，Linus一直使用diff和patch管理着Linux的代码。
* diff仟与patch是用于源码版本控制中的两个最基本的概念。

1. diff：
   ![diff](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/257291021239500.png)
2. patch：
   ![patch](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/210051421236055.png)
3. RCS：最早期的版本控制工具
   ![rcs](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/511491521231809.png)
4. CVS&SVN：集中式版本控制工具
   ![CVS&SVN](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/287421621249689.png)
5. GIT：LINUS的第二个伟大作品
   ![GIT](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/329642021247293.png)
   ![集中式](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/267013521243612.png)
    ![集中式/分布式](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/499722421244795.png)![对比](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/100282721226036.png)
    ![如何选择？](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/381502821248476.png)
   小结：什么是GIT？
    Git是一个版本控制工具，而且是一个开源的分布式版本控制工具。按照Lius本人的描述，Git的很多命令设计是来源于BitKeeper,但是Git有更多属性：

* 极快的速度,简单的设计
* 对非线性开发模式的强力支持（允许成干上万个并行开发的分支）
* 完全分布式
* 有能力高效管理类似Liux内核一样的超大规模项目（速度和数据量)

![本章小结](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/420694021237158.png)

### GIT安装与配置

#### linux下安装git

##### 包管理器方式安装

###### Linux系统：Ubuntu 10.10(maverick)或更新版本，Debian(squeeze)或更新版本

```
sudo aptitude install git
sudo aptitude install git-doc git-svn git-email gitk
```

其中gt软件包包含了大部分Gt命令，是必装的软件包。软件包git-svn、git-email、.gitk本来也是Git软件包的一部分，但是因为有着不一样的软件包依赖（多perl模组，tk等)，所以单独作为软件包发布。

###### Linux系统：RHEL、Fedora、CentOS等版本：

```
yum install git
yum install git-svn git-email gitk
```

##### 从源代码开始安装

访问Git的官方网站：http:/git-scm.com/。下载Git源码包，如：git-2.19.0.tar.gz,展开源码包，并进入到相应的目录中。

```
tar -jxvf git-2.19.0.tar.bz2
cd git-2.19.0
```

安装方法写在INSTALL文件当中，参照其中的指示完成安装。下面的命令将Git安装在/usr/local/bin中。

```
make prefix=/usr/local all
sudo make prefix=/usr/local install
```

安装Git文档（可选）。

```
make prefix=/usr/local doc info
sudo make prefix=/usr/local install-doc install-html install-info
```

##### 命令补齐

Linux的shell环境(bash)通过bash-completion软件包提供命令补齐功能，能够实现在录入命令参数时按一下或两下TAB键，实现参数的自动补齐或提示。例如输犬git com后按下TAB键，会自动补齐为git commit。
    
将Git源码包中的命令补齐脚本复制到bash-completion对应的目录中：

```
cp contrib/completion/git-completion.bash /etc/bash_completion.d/
```

重新加载自动补齐脚本，使之在当前shel|中生效：

```
$./etc/bash_completion
```

为了能够在终端开启时自动加载bash_completion脚本，需要在本地配置文件~/.bash_profile或全局文件
/etc/bashrc文件中添加下面的内容：

```
if [-f /etc/bash_completion ]then
/etc/bash_completion
fi
```

#### Windows下安装Git

目前Git提供的Windows安装包自带MinGW(Minimalist GNU for Windows,最简GNU工具集)，在安装后MinGW提供了一个bash提供的shell环境(Git Bash)以及其他相关工具软件，组成了一个最简系统(Minimal SYStem),这样在Git Bash中，Git的使用和在Linux下使用完全一致。

##### step1 下载git

go：https://git-scm.com/download/win ，下载对应windows版本的git![git web](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/500784622230292.png)

##### step2 安装git

git 需要选择一些组件，是否勾选上百度查，如：https://zhuanlan.zhihu.com/p/242540359

##### 安装TortoiseGit

在Windows下安装和使用Git有两个不同的方案，除了刚网刚的Git安装包，再有一个就是基于msysGit的**图形界面工具**一TortoiseGit。网络教程：https://www.cnblogs.com/houxianzhou/p/15386043.html![tortoisegit](https://gitee.com/wwwwaaa/web_img/raw/master/_posts/雏鹰计划c语言.md/129091523220822.png)

#### Git基本配置

Git有三种配置，分别以文件的形式存放在三个不同的地方。可以在命令行中使用git config工具查看这些变量。
系统配置（对所有用户都适用）

存放在git的安装目录下：%Git%/etc/gitconfig:若使用git config时用--system选项，读写的就是这个文件：

```
git config --system core.autocrlf
```

用户配置（只适用于该用户）

存放在用户目录下。例如linux存放在：~/.gitconfig:若使用git config时用-global选项，读写的就是这个文件：

```
git config --global user.name
```

仓库配置（只对当前项目有效）
当前仓库的配置文件（也就是工作目录中的.git/config文件）；若使用git config时用-local选项，读写的就是这个文件：

```
git config --local remote.origin.url

```

注：每一个级别的配置都会覆盖上层的相同配置，例如.git/config里9配置会覆盖%Git%/etc/gitconfig中的同名变量。

##### 配置个人身份

首次的Gt设定（设定身份，自己做主）

```
git config --global user.name “Zhang San”
git config --global user.email zhangsan123@huawei.com
```

这个配置信息会在Gt仓库中提交的修改信息中体现，**但和Git服务器认证使用的密码或者公钥密码无关**。

**注意事项**：
行不更名，坐不改姓：责任追踪/应用之间的用户关联/贡献度统计

##### 文本换行符配置

​	假如你正在windows.上写程序，又或者你正在和其他人合作，他们在Windows上编程，而你却在其他系统上，在这些情况下，你可能会遇到行尾结束符问题。这是因为**Windows使用回车和换行两个字符来结束一行**，而Mac和Linux只使用换行一个字符。虽然这是小问题，但它会极大地扰乱跨平台协作。
​	Git可以在你提交时自动地把行结束符CRLF转换成LF,而在签出代码时把LF转换成CRLF。用core.autocrlf来
打开此项功能，如果是在Windows系统上，把它设置成true,这样当签出代码时，LF会被转换成CRLF:

```
git config --global core.autocrlf true

```

​	Linux或Mac系统使用LF作为行结束符，因此你不想Git在签出文件时进行自动的转换；当一个以CRLF为行结束符的文件不小v心被引入时你肯定想进行修正，把core.autocrlfi设置成input来告诉Git在提交时把CRLF转换成LF,签出时不转换：

```
git config --global core.autocrlf input

```

​	这样会在Windows系统上的签出文件中保留CRLF,会在Mac和Linux系统上，包括仓库中保留LF。如果你是Wiindows程序员，且正在开发仅运行在Vindows.上的项目，可以设置false取消此功能，把回车符记录在库中：

```
git config --global core.autocrlf false


```

##### 文本编码配置

- **i18n.commitEncoding**选项：用来让git commit log存储时，采用的编码，默认UTF-8.

- **i18n.logOutputEncoding**选项：查看git log时，显示采用的编码，建议设置为UTF-8.

- 中文编码支持

  ```
  git config --global gui.encoding utf-8
  git config --global i18n.commitencoding utf-8
  git config --global i18n.logoutputencoding utf-8
  ```

- 显示路径中的中文：

  ```
  git config --global core.quotepath false
  ```

  

##### 与服务器的认证配置

###### 常见的两种协议认证方式

- http/https协议认证

​		设置口令缓存：

```
	git config --global credential.helper store

```

​		添加HTTPS证书信任：

```
	git config http.sslverify false
```


​		

- ssh协议认证

​	SSH协议是一种非常常用的Gt仓库访问协议，使用公钥认证、无需输入密码，加密传输，操作便利又保证安全性

###### ssh认证的配置过程

**生成公钥：**
	Git工具安装成功后运行Git Bash,在弹出的客户端命令行界面中输入下面提示的命令。
(比如你的邮箱是zhangsan1123@Huawei..com)

```
ssh-keygen -t rsa -C zhangsan1123@huawei.com
```

![image-20220401151729306](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204011517462.png)

**添加公钥到代码平台：**
	1.登录代码平台
	2.进入"Profile Settings"
	3.点击左侧栏的“SSH Keys”
	4.点击“Add SSH Key”,将刚生成的公钥文件的内容，复制到“Public Key栏，保存即可。



### Git的基本命令

#### Git版本控制下的三种工程区域&文件状态

![image-20220401154449420](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204011544517.png)

![image-20220401154457112](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204011544187.png)

#### Git常用命令

![image-20220401154619065](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204011546192.png)![image-20220401154632471](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204011546606.png)

#### Git常用命令实操

##### git init/git clone

​	**git init**用于在本地目录下新建git项目仓库。
​	执行git init后，当前目录下自动生成一个名为.git的目录，这代表当前项目所在目录已纳入Git管理。.git目录下存放着本项目的Git版本库，在此强烈不建议初学者改动.gt目录下的文件内容。
​	下图可知，Gt仓库下的.gt目录默认是不可见的，有一定程度上是出于防止用户误操作考虑。

![image-20220401154816293](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204011548405.png)

​	**git clone**用于克隆远端工程到本地磁盘。
​	如果想从远端服务器获取某个工程，那么：
​	1.确定自己Gt账号拥有访问、下载该工程的权限
​	2.获取该工程的Git仓库**URL**
​	3.本地命令行执行**git clone [URL]** 或**git lfs clone [URL]**

![image-20220401155022811](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204011550905.png)

​	**如果你所在的项目git服务器已支持git-lfs,对二进制文件进行了区别管理，那么克隆工程的时候务必使用品git lfs clone。否则克隆操作无法下载到工程中的二进制文件，工程内容不完整**

##### git add/git rm/git mv

###### git add

​	在提交你修改的文件之前，需要**git add**把文件**添加到暂存区**。
​	如果该文件是新创建，尚未被git跟踪的，需要先执行git add将该文件添加到暂存区，再执行提交。如果文件已经被git追踪，即曾经提交过的，在早期版本的git中，需要git add再提交；**在较新版本的git中，不需要git add即可提交**。

###### git rm 

​	**git rm**将指定文件彻底从当前分支的缓存区删除，因此它从当前分支的下一个提交快照中被删除。                                                                             	如果一个文件被git rm后进行了提交，那么它将脱离git跟踪，这个文件在之后的节点中不再受git工程的管理。

​	执行git rm后，该文件会在**缓存区消失**。你也可以直接从硬盘上删除文件，然后对该文件执行git commit,git会自动将删除的文件从索引中移除，效果一样。

###### git mv 

​	**git mv命令用于移动文件，也可以用于重命名文件。**
​	例1：需要将文件codehunter_.nginx.conf从当前目录移动到config目录下，可执行：

```
git mv codehunter_nginx.conf config
```

​	例2：需要将文件codehunter_nginx.conf重命名为new_nginx.conf,可执行：

```
git mv config/codehunter_nginx.conf config/new_nginx.conf
```



##### git diff/git status

###### git diff

​	用于比较项目中任意两个版本（分支)的差异，也可以用来比较当前的索引和上次提交间的差异)![image-20220401163438772](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204011634054.png)

###### git status

​	git status命令用于**显示工作目录和暂存区的状态**。使用此命令能看到修改的git文件是否已被暂存，新增的文件是否纳入了git版本库的管理。
下例中的信息表明：modeules./_init.py已被修改并暂存，LICENSE已被修改但未暂存，README.md已被删除但未暂存，extend.txt已被新建但未跟踪。注意，请保证能理解git status回显的每一行文字含义。![image-20220401163547075](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204011635240.png)

##### git commit

​	git commit主要是**将暂存区里的文件改动提交到本地的版本库**。
​	在此强调，提交这个动作是本地动作，是往本地的版本库中记录改动，不影响远端服务器。git commit 一般需要附带提交描述信息，所以常见用法是：

```
git commit file_name -m “commit message"
```

如果要**一次性提交所有**在暂存区改动的文件到版本库，可以执行：

```
git commit -a m “commit message”
```



##### git log

​	git log 用于查看提交历史

​	默认加其他参数的话，git log会按提交时间由近到远列出所有的历史提交日志。每个日志基本包含提交节点、作者信息、提交时间、提交说明等。
常用的日志命令格式：g1tlog

​	git log配合不同参数具有相当灵活强大的展示功能，常见的如一name-status/-p/-pretty/-graph等等，有兴趣的同学课后自行了解。![image-20220402152511848](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204021525280.png)

##### git push

​	在使用git commit命令将自己的修改从暂存区提交到本地版本库后，可以使用git push将本地版本库的分支推送到远程服务器上对应的分支。
​	成功推动远端仓库后，其他开发人员可以获取到你新提交的内容。
常用的推送命令格式：

```
git push origin branch_name
```

​	branch_name决定了你的本地分支推送成功后，在**远端服务器上的分支名**，其他人据此可以获取该分支上的改动内容。windows上分支名大小写不敏感，如master和Master是同一个分支。
​	你的本地分支名可以**与推送到远端的分支名不同**：

```
git push origin branch_name:new_branch_name

```

##### 分支管理 git branch/git checkout/git pull

###### git branch 

​	下图可见，git返回了当前本地工程所有的分支名称，其中master分支前面的"*”表示一当前工作区所在的分支是master。

.![image-20220402153315955](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204021533027.png)

​	如果想查看**远端服务器**上拥有哪些分支，那么执行**git branch -r**即可，返回的**分支名带origini前缀，表示在远端**;
​	如果想查看**远端服务器和本地工程所有的分支**，那么执行git branch -a即可。

![image-20220402153449489](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204021534568.png)

​	**git branch和git checkout-的异同：**
​	相同点：
​	git branch和git checkout-b都可以用于新建分支（默认基于当前分支节点创建）。
​	区别点：
​	git branch 新建分支后并不会切换到新分支，
​	git checkout-b新建分支后会**自动切换到新分支**。
​	常用的新建分支命令格式：

```
	git branch new_branch_name/git checkout-b branch,_name
```

![image-20220402153639813](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081910787.png)

​	git branch-d和git branch-D都可以用来删除本地分支，后者大写表示强制删除。
​	有时候当事分支上包含了未合并的改动，或者当事分支是当前所在分支，则-d无法删除，需要使用强制删除来达到目的。
​	常用的删除分支命令格式：

```
git branch-d branch_name/git branch-D branch_name
```

​	删除服务器上的远程分支可以使用git branch -d -r branch_name,其中 branch_name为本地分支名。
​	删除后，还要推送到服务器上才行，即 git push origin branch_name

![image-20220402154022537](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204021540645.png)

###### git checkout

​	git checkout命令除了创建分支，**还用来切换分支**，当然比较官方的叫法是“检出”。
​	有时候，当前分支工作区存在修改而未提交的文件，与目的分支上的内容冲突，会导致checkout切换
失败，这时候，可以使用**git checkout -f**进行强制切换。
​	常用的切换分支命令格式：git checkout branch_name
​	git checkout对象可以是分支，也可以是某个提交节点或者节点下的某个文件。建议课后自行按需了解。

<img src="https://gitee.com/wwwwaaa/web_img/raw/master/img/202204021542889.png" alt="image-20220402154247807"  />



###### git pull 

​	git pull的作用是，**从远端服务器中获取某个分支的更新，再与本地指定的分支进行自动合并**。
常用的切换分支命令格式：

```
	git pull origin remote_branch:local_branch
```

​	如果远程指定的分支与本地指定的分支相同，则可直接执行

```
	git pull origin remote_branch
```

###### git fetch

​	git fetch的作用是，**从远端服务器中获取某个分支的更新到本地仓库**。
​	注意，与git pull不同，git fetch在获取到更新后，**并不会进行合并**（即下页中的git merge)操作，
这样能留给用户一个操作空间，确认git fetch内容符合预期后，再决定是否手动合并节点。
​	常用的获取远端分支更新命令格式：

```
git fetch origin remote_branch:local_branch
```

​	如果远程指定的分支与本地指定的分支相同，则可直接执行

```
git fetch origin remote_branch
```



##### 分支合并git merge/git rebase

###### git merge

​	git merge命令是**用于从指定的分支（节点）合并到当前分支的操作**。
​	git会将指定的分支与当前分支进行比较，找出二者最近的一个共同节点base,之后将指定分支在base之后
分离的节点合并到当前分支上。分支合并，实际上是分支间差异提交节点的合并。
​	常用的合并分支命令格式：

```
	git merge branch_name
```

**实例**1：本地存在两个分支：master和master-2，

- master中存在的txt文件：a,b,c,**master-1**

- master-2中存在的txt文件:a,b,c,**d,master-2**

- 两个分支中的a,b,c均为空文件。

- 在master分支下执行命令： git merge master-2

  ![image-20220408130651321](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081306396.png)

- 发现：在master分支下，新增了两个txt文件：master-2,d，因为这两个文件是master分支中没有的。

**实例2**：

- 在实例1的基础上，master-2修改a.txt文件，在第一行增加:6666
- 在master分支下执行merge命令
- ![image-20220408131717717](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081317799.png)
- 查看master分支下的a.txt，发现该文件的第一行也增加了6666
- ![image-20220408131851330](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081318425.png)

**实例3**：

- 在实例1的基础上，修改master->b.txt:为![image-20220408133221907](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081912559.png)

- 修改master-2->b.txt为:![image-20220408133309611](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081333669.png)

- 在master分支下执行merge命令：提示出现了**内容冲突**，两个文件的**同一块区域**发生内容冲突

- ![image-20220408133410117](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081334206.png)

- 此时master->b.txt的内容为:

- ![image-20220408133832955](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081912907.png)

- 但在git pull 后，远端的分支只会受到8888 9999内容，不会把冲突内容提交

- ![image-20220408134330619](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081343699.png)

- **解决冲突：**

  - 按照实际需求修改master->b.txt

    ![image-20220408140339778](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081910851.png)

  - 依次执行 add,commit命令

    ![image-20220408140551711](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081910721.png)

  - 此时MERGING标志消失，冲突解决

    ![image-20220408140525496](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081910389.png)

###### git rebase

​	git rebase用于**合并目标分支内容到当前分支**。
​	git rebase这条命令用于分支合并，git merge也是用于分支合并。如果你要将其他分支的提交节点合并到
当前分支，那么git rebase和git merge都可以达到目的。

常用的合并命令格式：

```
git rebase branch_name
```

​	git rebase、git merge背后的实现机制和对合并后节点造成的影响有很大差异，有
各自的风险存在，暂不在本课中展开。

##### 强制回退到历史节点git reset/git checkout

###### git reset 

​	git reset通常用于**撤销当前工作区中的某些git add,/commit操作**，可将工作区内容回退到历史提交节点。
常用的工作区回退命令格式：git reset commit_id

![image-20220402155515599](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204021555815.png)

###### git check out

​	git checkout.用于回退本地**所有修改而未提交的文件内容**(没有commit的内容)。
​	git checkout这是条有风险的命令，因为它会取消本地工作区的修改（相对于暂存区），用暂存区的所
有文件直接覆盖本地文件，达到回退内容的目的。但它不给用户任何确认机会，所以谨慎使用。

​	常用的回退命令格式：

```
	git checkout .
```

​	如果仅仅想**回退某个文件**的未提交改动可以使用git checkout -filename来达到目的；如果想将工具区回退（检出）到某个提交版本，可以使用git checkoutcommit_id。

**实例**：

![image-20220408143312389](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081910633.png)

## 华为C语言编程规范

### 代码风格

#### 命名

##### 标识符命名应符合阅读习惯

​	好的代码可以读出来。

![image-20220408162151714](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081910590.png)

##### 作用域越大，命名越精确

结构体的成员冗余修饰太多，和结构体命的重合度大。![image-20220408162421918](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081910124.png)

![image-20220408162448163](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081911491.png)

##### 不推荐“匈牙利”风格

​	匈牙利风格：变量名=属性+类型+对象描述。

​	例如：m_lpszStr, 表示指向一个以0字符结尾的字符串的长指针 成员变量(m_)。

​	不推荐的原因：名字中包含信息，当信息改变时，名字如果改不了，会误导其他人，影响后续的维护工作。

##### 推荐统一使用驼峰命名法

- 小驼峰式命名规则：firstName, camelCase
- 大驼峰式命名规则：FirstName, CamelCase

|                          类型                          | 命名风格                                        | 形式                      |
| :----------------------------------------------------: | ----------------------------------------------- | ------------------------- |
|                                                        |                                                 |                           |
|      函数，结构体，枚举，联合体，typedef定义类型       | 大驼峰，或带模板前缀的大驼峰                    | AaaBbb，XXX_AaaBbb        |
| 局部变量，函数参数，宏参数，结构体中字段，联合体中成员 | 小驼峰                                          | aaaBbb                    |
|                        全局变量                        | 带'g_'前缀的小驼峰                              | g_aaaBbb                  |
|          宏(不包括函数式宏)，枚举值，goto标签          | 全大写，下划线分割                              | AAA_BBB                   |
|                        函数式宏                        | 全大写，下划线分割/大驼峰，或带模板前缀的大驼峰 | AAA_BBB,AaaBbb,XXX_AaaBbb |
|                          常量                          | 全大写，下划线分割/带'g_'前缀的小驼峰           | AAA_BBB,g_aaaBbb          |
|                                                        |                                                 |                           |

​	

#### 注释

##### 按需注释，全面不冗余

没有注释的情况：代码读不懂

注释太多：会弱化有效注释

好的注释：

![image-20220408181121200](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081811310.png)

##### 不要复制粘贴写注释

​	注释没有关键信息，注释有冗余，或者注释不完成。

![image-20220408181534440](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081815553.png)

##### 更多条款

- 注释符与注释内容间要有1空格
- 代码注释置于对应代码的上方或右边
- 文件头注释包含版权说明
- 不写空有格式的函数头注释

#### 格式

##### 写风格统一的代码

##### 行宽和可读性

- 行宽一般不超过120个字符。

- 如何考虑换行？考虑可读性，适当对齐。

  ![image-20220408182357248](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081911170.png)

##### 更多条款

- 非纯ASCII码源文件使用UTF-8编码
- 使用空格进行缩进，每次缩进4个空格
- 使用统一的大括号换行风格
- 行只有一条语句
- 行宽不超过120个字符
- 换行时将操作符留在行末，新行缩进一层或进行同类对齐
- 函数的返回类型及修饰符与函数名同行
- 条件、循环语句使用大括号
- ase/default语句相对switch缩进一层
- 指针类型"“跟随变量或者函数名
- 用空格突出关键字和重要信息
- 避免连续3个或更多空行

### 编程实践

#### 预处理

##### 常见不好宏的封装

![image-20220408182846649](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081828771.png)

##### 更多条款

- 使用函数代替函数式宏
- **定义宏时，要使用完备的括号**
- **包含多条语句的函数式宏的实现语句必须放在do-while(O)**
- **禁止把带副作用的表达式作为参数传递给函数式宏**
- 函数式宏定义中慎用return、goto、continue、break等改变程序流程的语句
- 函数式宏要简短
- **宏的名称不应与关键字相同禁止宏调用参数中出现预编译指令**
- 宏定义不以分号结尾
- **宏定义不应依赖宏外部的局部变量名**
- **if或#elif预处理指令中的常量表达式的值应为布尔值**
- **#if或#elif预处理指令中的常量表达式被求值前应确保其使用的标识符是有效的**
- **所有#else、#elif、#endif和与之对应的#if、#ifdef、#ifndef预处理指令应出现在同一文件**

#### 头文件

##### 头文件的作用

- 头文件在编译时被文本替换展开。
- 从而实现代码复用，使用同一套接口（声明），遵从相同的“约定”。

- 头文件是用于**被别人包含**的。
- 头文件的依赖关系，**体现了架构设计**。

- 错误观点：
  - .c是函数的堆叠，其他所有写到.h中
  - h是为了解决“使用前未声明”问题的
  - 源文件过大，拆分一个“私有头文件”出来

##### 头文件应当自包含

​	该包含的必须包含，源文件中没出现的符号必须在头文件中。

![image-20220408183738017](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081911249.png)

##### 破坏结构的“万能头文件”

![image-20220408183905886](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081839983.png)

##### 更多条款

- **在头文件中声明需要对外公开的接口**
- 头文件的扩展名只使用.h,不使用非习惯用法的扩展名，如.ic
- **禁止头文件循环依赖**
- **禁止包含用不到的头文件**
- **头文件应当自包含**
- **头文件必须用#define保护，防止重复包含**
- **禁止通过声明的方式引用外部函数接口、变量**
- **禁止在extern"C"中包含头文件**
- 按照合理的顺序包含头文件

#### 数据类型

##### 使用恰当的基本类型作为操作符的操作数

- Y表示允许，N表示不允许。		

![image-20220408184635921](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081911266.png)

##### 更多条款

- 不要重复定义基础类型
- 使用恰当的基本类型作为操作符的操作数
- 使用合适的类型表示字符

#### 常量

##### 禁止使用小写字母“i”作为数值型常量后缀

##### 不要使用难以理解的常量

![image-20220408185756337](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081857471.png)

#### 变量

##### 变量操作不当带来的风险

- 指针p1没初始化，指向内容不确定

  ![image-20220408190003135](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081900250.png)


##### **不要在子作用域中重用变量名**

**一个作用域包含另一个作用域时，不要在这两个作用域中使用相同的变量名**，例如：
如果某变量位于全局变量的子作用域内，则这个变量不应与全局变量重名。
语句块中定义的变量不能与包含该语句块的任何块中定义的变量重名。
重用变量名会使程序员对正在修改哪个变量感到困惑。此外，如果变量名被重用，则说明该变量名可能太通用,应使用更具有描述性的变量名。

![image-20220408191351919](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081913045.png)

##### 慎用全局变量

- 尽量少用全局变量

  - 尽量集中使用
  - 尽量封装

  ![image-20220408191611801](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081916905.png)

- 尽量封装全局变量

  ![image-20220408191642646](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081916762.png)

  ##### **资源不再使用时应予以关闭或释放**

- 这里的资源包括计算机内存文件描述符、socket描述符。

- 程序员在创建或分配资源后，如果该资源不再被使用，程序员应将其正确的关闭或释放，尤其要注意所有可能的异常路径避免遗漏。

- 以计算机内存为例，当动态分配内存不再使用时应予以释放，否则会发生**内存泄漏**，如果攻击者可以有意触发该漏洞，则内存资源可能被耗尽，造成拒绝服务。

- 以文件描述符为例，当打开的文件不再使用时应将指向该文件的描述符关闭，否则会发生该文件描述符泄漏，如果攻击者可以有意触发该漏洞，则可用的文件描述符可能被耗尽，造成**拒绝服务**。

![image-20220408191953587](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204081919705.png)

##### 更多条款

- **禁止使用未经初始化的变量**
- **不要在子作用域中重用变量名**
- 避免大量栈分配
- 慎用全局变量
- 指向资源句柄或描述符的变量，在资源释放后立即赋予新值
- **禁止将局部变量的地址返回到其作用域以外**
- 避免将只在一个函数中使用的变量声明为全局变量
- **资源不再使用时应予以关闭或释放**

#### 表达式

##### **执行算术运算或比较操作的两个操作数要求具有相同的基本类型**

- 在执行算术运算或者比较操作时，允许不同类型操作数之间进行转换，包括显式转换和隐式转换，这对程序员来说自由度是相当大的。
- 然而，不管是显式转换还是隐式转换，均可能会导致以下几个问题：数值丢失、符号丢失、精度丢失、布局丢失。
- 这就要求程序员必须明确识别类型转换之间的任何潜在风险，而显示转换肯定比隐式转换更容易识别风险，相同的基本类型之间的算术运算或比较操作肯定比显示转换更加容易被识别风险。

![image-20220408233525885](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204082335341.png)

##### **含有变量自增或自减运算的表达式中禁止再次引用该变量**

- 含有变量自增或自减运算的表达式中，如果再引用该变量，其结果在C语言标准中未明确定义。不同编译器或者同一个编译器不同版本实现可能会不一致。
- 为了更好的可移植性，不应该对标准未定义的运算次序做任何假设。
- 注意，运算次序的问题不能使用括号来解决，因为这不是优先级的问题。

![image-20220408233646373](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204082336714.png)

##### 更多条款

- **执行算术运算或比较操作的两个操作数要求具有相同的基本类型**
- 表达式的比较，应当遵循左侧倾向于变化、右侧倾向于不变的原则
- **含有变量自增或自减运算的表达式中禁止再次引用该变量**
- 用括号明确表达式的操作顺序，避免过分依赖默认优先级
- **不要向sizeof传递有副作用的操作数**
- 避免对带位域的结构体的布局做任何假设

#### 控制语句

##### **判断条件中的表达式的结果必须是布尔值**

![image-20220408234014573](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204082340066.png)

##### **循环必须安全退出**

- 否则有可能造成内存泄露。

![image-20220408234109063](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204082341488.png)

##### 每个switchi语句中至少有两个条件分支

- 左侧还不如用if,else语句，因为没有至少两个条件分支，使用switch太奢侈。

![image-20220408234208402](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204082342731.png)

##### 更多条款

- **判断条件中的表达式的结果必须是布尔值**
- &&和操作符的右侧操作数不应包含副作用
- **循环必须安全退出**
- **禁止使用浮点数作为循环计数器**
- 慎用goto语句
- **goto语句只能向下跳转**
- **switch语句要有default分支**
- 每个switchi语句中至少有两个条件分支

#### 声明与初始化

##### 不要声明或定义保留的标识符

如果声明或者定义了一个保留的标识符，那么程序的行为是未定义的。
1、**双下划线开头**，或**下划线加一个大写字母**开头的所有标识符始终保留使用；
2、除label名字和结构体、联合的成员外，其他以下划线开头的标识符都在文件范围内有效；
3、C标准库中已经定义的标识符均保留其用途；
4、errno和其他C标准库的外部链接标识符始终作为外部链接：
5、C标准库中具有文件范围的标识符都保留用作宏的名字。

#### 整数(要求严格)

##### 常见的整数操作问题

- **整数操作不当的几种场景**
  - 有（无）符号整数运算操作出现溢出（回绕）
  - 整型转换时出现截断错误
  - 有符号整数使用位操作符运算
- 整数操作不当带来的风险
  - 内存分配出错
    - 用户输入在与有符号的值和无符号的值之间的隐式转换进行交互时会产生一些错误，而这些错误经常会导致内存分配函数出现问题。
  - 执行任意代码
    - 整数溢出错误，可导致内存破坏，并可能被用于执行任意代码。

##### 整数使用的典型错误

###### 有符号数溢出

- 溢出会造成未定义行为。
- 举例：两数相加造成溢出，需先通过其他方式判断是否会溢出。

![image-20220408235938476](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204082359789.png)

###### 无符号数回绕

- 举例：两符号数相加，可能会回绕为0或其他较小的数据。

![image-20220409000121194](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090001620.png)

- 举例：乘法运算，先运算后赋值，必须先用更大的类型包含结果，如下图中的unit_32转unit_64

![image-20220409000233917](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090002246.png)

###### 截断

![image-20220409000653512](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090006056.png)

###### 符号错误

![image-20220409000722753](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090007134.png)

###### 移位错误

![image-20220409000921831](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090009234.png)

![image-20220409001222238](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090012580.png)

###### 除零错误

- 除数为0.会造成未定义行为。

![image-20220409001303103](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090013446.png)

##### 更多条款

- **确保有符号整数运算不**<mark>溢出</mark>
- **确保无符号整数运算不<mark>回绕</mark>**
- **确保除法和余数运算不会导致除零错误（被零除）**
- **整型表达式比较或赋值为一种更大类型之前必须用这种更大类型对它进行求值**
- **只能对无符号整数进行位运算**
- **校验外部数据中整数值的合法性**
- **移位操作符的右操作数必须是非负数且小于左操作数的基本类型位宽**
- 表示对象大小的整数值或变量应当使用size_t类型
- **确保枚举常量映射到唯一值**
- **确保整数转换不会造成数据截断或符号错误**

#### 指针与数组

##### 数组操作介绍

![image-20220409001743640](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090017045.png)

##### 典型错误

###### 差一错误

- 数组越界。

![image-20220409002035961](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090020300.png)

##### sizeof使用错误

- sizeof操作符得到的是调整后指针类型的大小。

![image-20220409002126943](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090021168.png)

##### 不同类型对象指针的强制转换

- 强制转换造成指针不对齐，导致未定义

![image-20220409002243025](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090022307.png)

##### 更多条款

- **外部数据作为数组索引时必须确保在数组大小范围内**
- **禁止通过对数组类型的函数参数变量进行sizeof:来获取数组大小**
- **禁止通过对指针变量进行sizeof操作来获取数组大小**
- 禁止整数与指针间的互相转化
- 不同类型的对象指针之间不应进行强制转换
- **不要使用变长数组类型**
- 声明一个带有外部链接的数组时，必须显式指定它的大小

#### 字符串

##### 字符串操作介绍

![image-20220409002415827](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090024104.png)

##### 字符串操作不当带来的风险

![image-20220409003343931](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090033501.png)

##### 条款

- **确保字符串存储有足够的空间容纳字符数据和null结束符**
- **对字符串进行存储操作，确保字符串有null结束符**

#### 断言

##### 断言常见问题

- 不使用断言
  - 断言用于对程序内部数据的检查，体现了程序员设计代码时的安全意识，同时，也以代码的形式将内部接口参数的约束直接呈现出来，增加了代码的可读性与可维护性。

- 断言在release版本中被触发，导致拒绝服务问题(CWE-617)

  - ·直接使用系统的assert(）:系统提供的assert()通常使用DEBUG宏隔离，但产品代码实际隔离调试版本的宏通常不是直接使用DEBUG宏，因此assert()将带入发布版本的二进制代码.
  - ·直接使用系统的BUG_ON():直接使用BUG_ON,将在release版本中完全放弃对系统资源的控制，直接导致拒绝服务降低了产品的可用性
  - ·使用断言检查运行时错误：运行时错误通过某种条件是可以被触发的，release版本需要正确处理这些错误，而断言在release版本中是无效的，因此不能使用断言检查此类错误.

  - 使用断言改变系统状态：如进行赋值，I/O操作等。由于在发布版本中断言是不起作用的，因此这些操作将无法生效

##### 条款

- **断言必须使用宏定义，且只能在调试版本中生效**
- 避免在代码中直接使用assert()
- **禁止用断言检测程序在运行期间可能导致的错误，可能发生的错误要用错误处理代码来处理**
- **禁止在断言内改变运行环境**
- **一个断言只用于检查一个错误**

#### 函数设计

##### 外部输入校验原则

![image-20220409005212727](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090052227.png)

- 举例
  - A模块foo校验，foo2相信foo传递的数据，不进行校验。
  - B模块的foo需要校验，因为没有理由信任A模块的数据。

![image-20220409005540516](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090055154.png)

##### 更多条款

- <mark>对所有外部数据进行合法性检查</mark>

- 函数应当合理设计返回值

- <mark>对象或函数的所有声明必须与定义具有一致的名称和类型限定符</mark>
- **函数的指针参数如果不是用于修改所指向的对象就应该声明为指向const的指针**
- 函数避免使用Void*类型参数
- 函数要简短
- 内联函数避免超过10行
- **数组作为函数参数时，必须同时将其长度作为函数的参数**
- 将字符串或指针作为函数参数时，在函数体中应检查参数是否为NULL
- 避免修改函数参数的值
- 函数只在一个文件内使用时应当使用static修饰符

#### 函数使用

##### 条款

- 函数返回值
  - **处理函数的返回值**

![image-20220409010126917](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090101097.png)

- 退出类函数（释放资源）
  - **禁用atexit函数**
  - **禁止调用kill、TerminateProcess函数直接终止其他进程**
  - **禁用pthread exit、ExitThread函数**
  - 除main函数以外的其他函数，禁止使用exit、ExitProcess函数退出
  - **禁用abort函数**

#### 格式化输入/输出函数

##### 格式化函数介绍

![image-20220409010454980](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090104537.png)

##### 格式化缺陷的风险

![image-20220409010702642](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090107968.png)

##### 存在格式化缺陷的函数，应慎用

![image-20220409011001473](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090110919.png)

##### 条款

- **调用格式化输入/输出函数时，禁止format参数受外部数据控制**
- **调用格式化输入/输出函数时，使用有效的格式字符串**

#### 内存分配函数

##### realloc调整内存大小

![image-20220409011317656](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090113917.png)

##### alloca()申请内存

![image-20220409011258691](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090112141.png)

##### 条款

- **禁止使用realloc(）函数**
- **禁止使用alloca()函数申请栈上内存**

#### 安全函数

##### 典型错误-重定义安全函数

![image-20220409011559726](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090116039.png)

##### 典型错我-封装安全函数

![image-20220409011741548](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090117963.png)

![image-20220409011747470](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090117808.png)

##### destMax参数的设置问题

- 典型错误-destMax参数设置

![image-20220409011947121](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090119445.png)

![image-20220409012013309](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090120720.png)

![image-20220409012117337](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090121703.png)

##### 安全函数返回值检查问题-不用检查的情况

- ![image-20220409012232916](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090122244.png)

- ![image-20220409012310182](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090123464.png)
- ![image-20220409012328596](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090123049.png)
- ![image-20220409012417635](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090124024.png)
- ![image-20220409012436260](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090124579.png)

##### 条款

- **必须检查安全函数返回值，并进行正确的处理**
- **正确设置安全函数中的destMax参数**
- **禁止封装安全函数**
- **禁止用宏重命名安全函数**
- **只能使用华为安全函数库中的安全函数或经华为认可的其他安全函数**

#### 文件函数

##### 条件竞争原理

- 尽量用文件句柄而不是文件名。因为文件句柄不可替换，在时间间隙内不会被攻击。

![image-20220409012618779](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090126165.png)

- 示例：

![image-20220409012730658](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090127182.png)

![image-20220409012745413](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090127730.png)

##### 目录遍历

![image-20220409012807554](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090128872.png)

- 示例

![image-20220409012836191](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090128577.png)

##### 典型错误-使用文件名的方式对文件进行操作

![image-20220409012915156](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090129375.png)

##### 典型错误-输入文件路径没有进行标准化

![image-20220409013105274](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090131720.png)

##### 条款

- **创建文件时必须显式指定合适的文件访问权限**
- **使用文件路径前必须进行规范化并校验**
- **不要在共享目录中创建临时文件**

#### 其他函数

##### 命令注入

![image-20220409013358262](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090133746.png)

##### 典型错误-system函数不当使用

![image-20220409013527112](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090135683.png)

##### 多线程下使用线程安全函数

![image-20220409013658676](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090136064.png)

- 典型错误-strtok使用

![image-20220409013808480](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090138857.png)

- 使用安全函数代替

![image-20220409013904440](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090139958.png)

- 典型错误-信号处理程序中使用异步不安全函数

![image-20220409014006107](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090140464.png)

![image-20220409014112347](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090141916.png)

##### 不安全函数的使用

![image-20220409014148978](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090141331.png)

![image-20220409014300050](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090143609.png)

##### 条款

- **禁止外部可控数据作为进程启动函数的参数**
- **禁止外部可控数据作为dlopen/LoadLibrary等模块加载函数的参数**
- **禁止直接使用外部数据拼接SQL命令**
- **禁止在信号处理例程中调用非异步安全函数**
- **使用库函数时避免竞争条件**
- **禁止使用内存操作类不安全函数**
- 避免使用atoi、atol、atol、atof函数

### 其他

#### 不安全随机数的使用

![image-20220409014530279](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090145588.png)

##### 预测随机数

![image-20220409014607024](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090146391.png)

![image-20220409014630730](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090146145.png)

##### 典型错误-使用不安全的随机数

![image-20220409014701737](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090147206.png)

![image-20220409014713696](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090147010.png)

#### 敏感信息清理

![image-20220409014831162](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204090148668.png)

#### 条款

- **删除无效或永不执行的代码**
- **不要在信号处理函数中访问共享对象**
- **禁用rand函数产生用于安全用途的伪随机数**
- **禁止在发布版本中输出对象或函数的地址**

## C语言进阶

### 变量

#### 变量基本类型

- 类型：char short int long float double

#### 变量内存大小

- ​	在32位系统中的大小，一般为：1,2,4,4,4,8

#### 无符号类型

- 是否能加unsigned?

  - 可以加：char,short,,int,long

  - 不可以：float,double

#### 变量定义

![image-20220410211448530](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102114481.png)

#### 数据模型

- 数据模型决定了基本数据类型的长度

![image-20220410211916862](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102119117.png)

- 下表统计的是各个基本数据数据类型在Window/Linux32/64位系统下的字节数。对于编程的时候需要注意标红色的部分。

![image-20220410211936652](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102119771.png)

### 表达式

#### 取词

![image-20220410212711645](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102127740.png)

- m4，m5,m10:++符号只能对变量进行操作
- m9:-(负的)+(正的)-3(负3)

​	![image-20220410213105015](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102131139.png)



#### 类型转换(32位和64位的区别)

##### 类的自动转换-1：

![image-20220410213314874](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102133988.png)

##### 类的自动转换-2：

- c：a+b->b先转换成无符号数,a-b=FFFFFF2,c为有符号数，a-b需要转换成有符号数，即-14

![image-20220410213923141](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102139268.png)

##### 类的自动转换-3:

- 原则：
  1.参与运算的类型不同，先转换成**相同类型**再运算。
  2.数据类型向数据长度增长的方向转换，**char->short**->int->unsigned int->long
  3.所有float都会先转成double进行运算，那怕只有一个float
  4.赋值运算时，赋值号右边的类型向左边的类型转换。
  5.浮点数和整形数，整形数向浮点数转换。
  6.在表达式中，如果char和short类型的值进行运算，无论char和short有无符号，结
  果都会自动转换成int。
  7.如果char或short与int类型进行计算，结果和int类型相同。
  即：如果int是有符号，结果就是带符号的，如果int是无符号的，结果就是无符号的。
  **数据类型排序：**
  long double,double,float,unsigned long long,long long,unsigned long,
  long,unsigned int,int,**unsigned short,short,unsigned char,char**
  注意：同一类型，**有符号数和无符号数运算，有符号数向无符号数转变**。

### 宏

### 常用功能

#### 常量替换

![image-20220410215708342](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102157461.png)

#### 类型定义

![image-20220410220859623](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102208730.png)

#### 函数宏

![image-20220410215757290](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102157402.png)

#### 防止头文件重复包含

![image-20220410220112703](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102201815.png)

#### 内定调试宏

![image-20220410220405225](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102204360.png)

### 风险说明

#### 运算符优先级引发的问题

![image-20220410220602306](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102206424.png)

![image-20220410220612345](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102206441.png)

#### 多次运算引发的问题

​	宏嵌套其他函数。

![image-20220410220723450](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102207566.png)

#### 同一个宏多个名字

![image-20220410220933125](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102209225.png)

#### 空格

![image-20220410221022942](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102210066.png)

### 宏的优点和不足

- 不足：
  - 1.宏在符号表中不存在，所以宏函数无法打热补丁，在s或者单板
  - 调试过程中，无法引用
  - 2.程序的代码空间增大
  - 3.宏函数单步调试时无法进入
  - 4.宏常量没有数据类型，编译器不能在编译时进行深入类型检查
- 优点：
  - 1.宏函数效率较高
  - 2.数组大小，只能在编译阶段确定，只能用宏
  - 3.其他前面讲的功能

### 枚举

### 枚举类型的大小

##### 案例说明

![image-20220410222849690](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102228813.png)

##### 注意事项

![image-20220410222925708](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102229822.png)

### 枚举的值

#### 案例说明

- 定义枚举类型enum，顺便定义了变量ENUM_1

![image-20220410223013208](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102230334.png)

#### 规则说明

![image-20220410223219529](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102232643.png)

### 使用建议

![image-20220410223240601](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102232700.png)

### 结构体

#### 结构体初始化

- a1.c默认值为0.

![image-20220410224623940](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102246039.png)

![image-20220410224730380](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102247492.png)



#### 结构体对齐

##### 什么是结构体对齐

- 字节对齐：现代计算机中内存空间都是按照byte划分的，从理论上讲似乎对任何类型的变量的访问可以从任何地址开始，但实际情况是在访问特定类型变量的时候经常在特定的内存地址访问，这就需要各种类型数据按照一定的规则在空间上排列，而不是顺序的一个接一个的排放，这就是对齐。
- 四字节对齐：4字节类型数据，希望从能被4整除的地址开始取操作数。

##### 为什么要结构体对齐

###### 运行速度更快

- STRU_1，取m需要寻址两次，另两个只需要一次

![image-20220410225056992](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102250098.png)



###### 指针从基地址读取数据可能出错	

![image-20220410225405363](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102254482.png)

###### 结构体直接赋值可能出错

![image-20220410225532938](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102255054.png)



##### 解决字节对齐的办法

![image-20220410225604660](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102256750.png)

#### 结构体大小计算

![image-20220410225848824](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102258933.png)

- 当累加起来不够时会优先合并补齐
  - 题中的flag3和flag4

![image-20220411154202411](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111542511.png)

- 数组没有占用结构体的空间

![image-20220410230113502](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102301605.png)



### 联合体

#### 基本用法

![image-20220410231210798](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102312113.png)

#### sizeof大小

- 取成员最大。
- 

![image-20220410231304750](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204102313015.png)

### 函数

#### 函数定义

![image-20220411135220844](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111352938.png)



#### 函数入参

- b,c,d：传入参数都是指针，传数组=传指针

![image-20220411135239694](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111352779.png)

#### inline函数

![image-20220411135333042](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111353144.png)

#### static函数

![image-20220411135351551](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111353898.png)

#### extern函数

![image-20220411135422848](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111354940.png)

### 数组

#### 指针和数组基本形态

![image-20220411135632860](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111356952.png)

![image-20220411135746841](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111357933.png)

![image-20220411135808697](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111358780.png)



#### 数组和指针不可互换场景

- sizeof
  - 指针永远是4，即右边m=4
  - 左边sizeof操作指针时，m为数组总字节数，m=6

![image-20220411135940462](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111359541.png)

#### 数组和指针可互换场景

- 函数参数

![image-20220411135856186](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111358275.png)

- 表达式

![image-20220411140327622](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111403717.png)

#### 多维数组和指针

- 步长

![image-20220411140530815](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111405917.png)

- 定义

![image-20220411140729784](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111407895.png)

- 函数形式参数

![image-20220411140909961](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111409054.png)

#### 数组和指针初始化

- 一维数组

![image-20220411141000349](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111410445.png)

### 内存分布

#### 案例说明

- 案例1：
  - 1,2:指向局部变量，函数结束内存收回，内容不可知
  - 3:指向常量"abc"的地址，可以输出
  - 4:static，函数退出后内存空间依然存在。

![image-20220411142334926](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111423023.png)

- 案例2：

  - 1.全局变量，初始化为0
  - 2.栈分配内存，数字随机，在debug版本有保护机制所以会出现程序崩溃
  - 3.堆分配内存，数字随机。

  ![image-20220411142744871](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111427986.png)

- 案例3:

![image-20220411142921486](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111429616.png)

#### 内存分布概述

![image-20220411143004279](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204111430393.png)



## 开发者测试

### 概述

#### 基础概念

- 开发者测试(DT)定义：面向开发者的测试，关注功能、接口的正确性
  - 开发者测试(Developer Test.,DT),是指开发者所做的测试，有别于专职测试人员进行的测试活动。**主要是PC级仿真测试，少量的真实环境测试**。
  - 除了少数联调用例，DT主要关注点是**自己开发的功能**是否OK,自己开发的功能可能是一小块，但是还是需要UT、IT、PCST、少量真实环境测试（业界UT：IT:ST比例是7：2：1，但需要结合业务特点选择合适比例，测试分层那门课有讲），目的是除了验证新开发的单元功能0k外，还要通过集成验证与周边的接口是否ok,以及是否破坏其他功能。

![image-20220412102834296](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121028228.png)

- 优势：

  ◆**对真实环境依赖低**
  	对真实环境依赖低，可基于通用PC构建测试环境，测试环境成本低。比如无线，不需要到真实基站测试。
  **◆场景构造更加灵活，可精确覆盖**
  	场景构造灵活，可以更精确的功能覆盖，包括异常场景。·
  **◆作为防护网，自动化程度高、反馈快，高效可信重构的前提**
  	自动化程度高，执行时间短，稳定性高，可快速全量持续集成（本地修改代码后快速运行全量测试用例、上库门禁快速运行全量测试用例)，保证代码的任何修改都能得到快速反馈，最大程度降低问题引入概率。
  **◆开发者测试发现的问题容易定位，解决成本低**
  ◆**开发者测试能有效降低前端遗留缺陷，提升代码交付质量，发布周期更快。**

局限性

◆**部分场景DT仿真成本高**
    比如多线程、定时器、系统启动等场景在DT下很难模拟，或者模拟成本很高（实现复杂、编译和运行时间过长等），这些场景就可以通过真实环境测试守护，通过合理的测试分层获得最大性价比。

开发者测试(DT)的价值：提前发现问题，自动化看护代码质量

- **测试前移，提前发现问题**

  - **后端问题解决的成本远大于前端**

  - 对开发者测试的要求：有测试框架技撑，能够高效的编写/调试/运行测试代码，测试框架能够支撑**85%**及以上场景测试

- **持续集成(CI)门禁**

  - 多人协同开发，采用分支开发合并到主干集成的方式，分支时间越长，代码越多集成到主干越困难，可能会出现编译失败、功能冲突等问题，敏捷推荐分支代码持续集成到主干，为了保证主干健康，需要门禁防护网（静态检查、动态检查），动态检查主要就是自动化测试用例，DT测试用例不上门禁，本地测得再好，集成到主干还是各种问题，所以DT测试用例必须上门禁
  - 对开发者测试的要求：满足门禁要求，**测试工程编译+运行5分钟左右**，测试用例自动化校验，行覆盖率80%以上

- **重构防护网**

  - 提升可维护性的核心思想就是**封装隔离变化**，但对于变化的识别很难做到100%准确，如果将所有可能出现的变化都封装隔离又会导致过度设计。基于**敏捷思想**，我们要简单设计，然后对新出现的变化进行封装隔离，所以要持续重构，重构要求每一步的功能不发生变化，除了重构16字真言（旧的不变，新的创建，一步切换，旧的再见）的指导外，还需要**每次修改完代码都运行一下测试用例**，确保功能不变。

  - 对开发者测试的要求：本地测试工程编译+运行足够高效(UT30s,T3分钟，PCST5分钟)，测试用例自动化校验

#### 可测试性

- 可测试性：软件发现故障并圆离、定位故障的能力，以及在一定的时间和成本前提下，进行测试设计、测试执行的能力。
- 可测试性好的代码，更容易被测试，测试出来的问题更容易定位，可以降低测试成本，提升测试质量。
- 可测试性主要包括可隔离性、可控制性、可观测性、可定位性。

![image-20220412103838271](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121038396.png)

- IBO模型：

![image-20220412104422548](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121044652.png)

#### 测试分层

- 测试分层原因：降低测试复杂的

![image-20220412104813578](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121048687.png)

- ![image-20220412105717448](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121057553.png)

### 测试设计方法

#### 测试设计概述

##### 概述

- 测试设计：基子被测对象的**测试依据**，通过一定的分析设计方法得到测试用例的活动。这些测试用例可以实现特定(与被测对象本身的风险程度相匹配)的**测试覆盖**。
- 测试设计着眼于如下三方面的问题：
  - 识别影响被测对象行为的测试因子
  - 避免遗漏通过合理的方法对测试因子的取值进行选取，并进行合理组合
  - 明确合理的预期结果
- 好的测试设计可以获得更优的测试性价比：在有限的时间内，高质量测试设计输出的测试用例往往能够更合理对被测对象的功能点&场景进行覆盖，满足交付质量要求。
- 虽然我们提倡测试性价比，但我们并不提倡在一个测试用例中测试过多的内容，每个用例应该有自己明确的测试目的。

##### 开发者测试所处的位置和大致过程

![image-20220412134047160](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121340276.png)

##### 测试设计的一般步骤

- 需求分析->因子识别->因子组合->确定预期

![image-20220412134159813](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121341904.png)

#### 常见的测试设计方法

##### 确定因子取值的技术

###### 划分等价类

![image-20220412134400794](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121344896.png)

###### 分析边界值

- 经验告诉我们，大量的错误是发生在输入或输出范围的边界上，边界值分析就是在划分的等价类区域的边界及其附近进行测试数据的选取。是对等价类划分的一种补充。
- 几条经验：
  - 1.如果输入条件规定了值的范围，则应取刚达到这个范围的边界的值，以及刚刚超越这个范围边界的值作为测试输入数据。
  - 2.如果输入是有序集合，则应选取集合的第一个元素和最后一个元素作为测试输入数据。
- 边界值的重要性：缺陷出现在边界的概率比较高

![image-20220412135236688](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121352795.png)

##### 因子组合技术

- 多个测试因子如何实现有效组合，从而降低验证成本

![image-20220412135934660](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121359757.png)

- AC-全组合

![image-20220412142154942](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121421049.png)

- BC-每次只改1个测试因子

![image-20220412142239267](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121422372.png)

- EC-每个测试因子在组合中至少出现一次

![image-20220412143639275](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121436388.png)

- Pair-Wise：每两个测试因子的取值组合至少覆盖一次

![image-20220412144039422](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121440521.png)

- N-Wise：每N个因子在组合中至少出现1次

![image-20220412144328630](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121443729.png)

#### 通过代码覆盖分析进行测试

- 代码覆盖评估方法：目的是基于覆盖结果完善测试设计与执行

![image-20220412144737077](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121447198.png)

### C/C++测试实现与执行

#### 测试框架概述

![image-20220412150646222](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121506320.png)

#### gtest测试框架

##### gtest事件机制：用于决定测试程序运行的顺序

![image-20220412150743252](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121507372.png)

- gtest示例：

![image-20220412151402414](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121514549.png)

##### gtest断言

![image-20220412152308066](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121523249.png)

##### gtest运行参数

![image-20220412152839850](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121528972.png)



#### gMock基础知识

- Mock：便捷的模拟对象方法
- Mock方法实例：

![image-20220412154952752](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121549906.png)

- Mock调用期待：用于设置测试程序预期的行为

![image-20220412155128843](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121551315.png)

- 测试用例：

![image-20220412155820684](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121558799.png)

- gMock匹配器

![image-20220412155933983](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121559088.png)

- gmock应用

![image-20220412161246200](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121612313.png)

- gMock进阶概念练习

![image-20220412161334006](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121613115.png)

#### GDB调试技能

##### 简介

- GDB是GNU开源组织发布的一个强大的UNX下的程序调试工具。或许，各位比较喜欢那种图形界面方式的，像VC、BCB等IDE的调试，但如果你是在UNIX平台下做软件，你会发现GDB这个调试工具有比VC、BCB的图形化调试器更强大的功能。所谓“寸有所长，尺有所短”就是这个道理
- 一般来说，GDB主要帮助你完成下面四个方面的功能：
  - 启动你的程序，可以按照你的自定义的要求随心所欲的运行程序。
  - 可让被调试的程序在你所指定的调置的断点处停住。
  - 当程序被停住时，可以检查此时你的程序中所发生的事。
  - 动态的改变你程序的执行环境。

##### GDB调试的三种方式

![image-20220412161907831](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121619966.png)

##### GDB指令介绍

![image-20220412161930787](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121619893.png)

### 开发者测试案例实践

无源代码。

## C++编程规范

### 代码风格

#### 使用统一命名风格	

![image-20220413133202315](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131332279.png)

#### 统一文件命名

- **C++源文件以.cpp为扩展名，头文件以.h为扩展名**
  - 如果当前项目组已经使用了业界其他的扩展名(.hh、hpp、.cc、.cxx等)，那么可以继续使用，并保持风格统一
  - C++20中推出的新特性Modules的扩展名，可以根据编译器的支持情况选择，例如：.cppm、,ixx等
- **C++文件名和类名保持一致**
  - C++的头文件和源文件名应该和类名保持一致，可以使用大驼峰或者小写下划线风格
  - 项目组选择一种风格约定执行，并保持风格统一
  - 如果有一个类叫DatabaseConnection,那么对应的文件名：
    - .DatabaseConnection.h
    - .DatabaseConnection.cpp
  - 或者如下：
  - .database_connection.h
  - .database_connection.cpp



#### 合理使用注释

![image-20220413133558241](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131335367.png)

#### 注释格式

![image-20220413133651865](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131336967.png)

#### 使用统一的大括号风格

![image-20220413133744484](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131337600.png)

#### 行宽不超过120字符

![image-20220413133831748](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131338862.png)

#### 其他排版格式

<img src="https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131338033.png" alt="image-20220413133854933" style="zoom: 200%;" />

### 编程实践

#### 函数

##### 输入校验原则

![image-20220413134432296](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131344414.png)

##### 合理选择参数与返回类型

- 函数参数：
  - ·对于输入参数，拷贝代价小的类型传值，拷贝代价大的类型传const引用
  - ·对于同时作为输入和输出的参数，传递非const引用
  - ·要被移动的参数，应使用右值引用类型，并在函数内使用std:move
  - 要被转发的参数使用“转发引l用”类型，并在函数内使用std:forward
  - ·总是要在函数内部产生副本并且移动代价较小的参数，可以按值传递
  - **避免定义C风格的变参函数**
  - **尽量避免使用void*参数**
  - **不应有未被使用的参数，挡由于某种原因需要保留时，应当将参数注释掉**
- 返回类型：
  - 设计函数时，优先使用返回值而不是输出参数
  - 返回多个值时，优先使用返回类型为struct或std:tuple
  - 不要使用std:move返回函数局部变量
- 特殊场景：
  - 当入参或返回值可能无效时，可以使用T*、std:optional或一个可以代表无效状态的对象

![image-20220413141033136](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131410250.png)

#### 类

##### 类的设计应该保证其封装性	

- **为了保证类的封装性，防止类和外部发生不必要的耦合**，应该满足下面的原则：
  - 类的成员变量应该声明为private,常量可以声明为public
  - friend可以用在操作符重载，其他场景中避免使用
  - 当成员变量可以独立进行变化时，可以使用struct,否则使用class
  - 类的oublic成员函数如果返回私有数据地址，则容易与外部造成数据耦合，应尽量避免
- 构造函数应该有一个契约：构造后的对像应该是完整的、可用的。在允许使用异常的情况下，应当使用异常来报告错误。

![image-20220413141233663](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131412779.png)

##### 类的成员变量必须显示初始化

- 类的所有构造函数都要保证所有成员被正确初始化
- 如果类的成员变量具有默认构造函数，那么可以不做显式初始化
- 优先使用初始化列表或者类内初始化来初始化成员

![image-20220413141430020](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131414137.png)

##### 明确需要实现哪些特殊成员函数

<img src="https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131415858.png" alt="image-20220413141554728" style="zoom:150%;" />

##### 继承，操作符重载

![image-20220413141909639](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131419772.png)

#### 变量

##### 尽量使用const

- 合理使用cost语义能够使代码更易于理解和维护。使用const修饰的变量、数据成员、函数以及函数参数为编译时类型检测增加了一层保障，便于尽早发现错误。参考如下原则：
  - 将初始化后不会再修改的成员变量定义为cost,可以防止无意间更改对象值而出现意外。
  - 对于指针和引用类型的参数，如果不需要修改其引用的对象，应使用cost修饰，明确表明在函数内部不会修改这个参数。
  - 对于不会修改成员变量的成员函数应使用const修饰。注意：对于实例化为const的对象，也只允许调用其const成员函数。
  - C++11之后优先使用constexpr定义在编译时就可以被计算出的值。
    - 下图展示constexpr的用法：

![image-20220413142631441](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131426568.png)

#### 枚举

- 如何使用枚举：

![image-20220413142841775](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131428912.png)

#### 声明与定义

##### 不要声明与定义保留标识符

![image-20220413143141301](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131431421.png)

![image-20220413143108830](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131431932.png)

##### 保持作用域尽量小

![image-20220413143205512](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131432632.png)

##### 遵循单一定义原则

![image-20220413144722997](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131447136.png)

##### 禁止逐位操作非 trivially copyable对象

![image-20220413144919571](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131449695.png)

#### 类型转换

##### 使用由C++提供的类型转换操作

- **C++提供的类型转换操作比C风格更有针对性，也更加安全。**
  - **dynamic_cast**:.主要用于继承体系下行转换，该转换具有类型检查的功能。dynamic cast的出现一般说明基类和派生类设计出现了问题，派生类破坏了基类的契约，不得不通过dynamic cast转换到派生类进行特殊处理，这时应当考虑是否需要优化设计。如果这种
    强制转换不可避免，**应优先使用dynamic cast,而不是使用static cast,除非转换的不是多态基类**
  - **static_cast**:和C风格转换相似可做值的强制转换，或者void*到其他指针类型的转换
  - **reinterpret_cast**:用于转换不相关的类型，强制将某个类型对象的内存重新解释成另一种类型，是一种不安全的转换，应尽量少用
  - **const_cast**:用于移除对象的const属性，使对象变得可修改，这样会破坏数据的不变性，只有在明确不会修改对象时，可以使用

![image-20220413145440707](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131454832.png)

##### 禁止使用std:move操作consti对象

- 字面上std:move的意思是要移动一个对象，而const对象是不允许修改的，自然也无法移动，会给代码阅读者带来困惑。代码实际功能往往退化成了对象拷贝而不是对象移动，带来了性能上的损失。

![image-20220413145912264](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131459377.png)

##### 常见的整数运算问题

![image-20220413150018569](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131500692.png)

- 整数溢出/回绕原理

![image-20220413150149945](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131501059.png)

- 可能引发溢出的操作

![image-20220413150323621](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131503733.png)

- 只对无符号数做位运算

![image-20220413150649515](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131506629.png)



#### 表达式

###### 使用恰当的基本类型作为操作符的操作数

![image-20220413150851664](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131508787.png)

###### 比较两个表达式时，左侧倾向于变化，右侧倾向于不变

![image-20220413150915865](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131509971.png)

###### 注意表达式的副作用

![image-20220413151008328](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131510438.png)

###### 使用static assert在编译期暴露问题

![image-20220413151235543](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131512655.png)

#### 语句

##### 控制表达式的结果必须是布尔值

![image-20220413151341072](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131513182.png)

##### switch语句

![image-20220413151410513](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131514625.png)

#### 资源管理

##### 资源访问校验

<img src="https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131515667.png" alt="image-20220413151529537" style="zoom:150%;" />

##### 在传递数组参数时，不应单独传递指针

![image-20220413151724231](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131517424.png)

##### 使用RALL技术管理资源的生命周期

![image-20220413151805003](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131518109.png)

##### 不要访问超出生命周期的对象

![image-20220413160608705](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131606826.png)

##### 合理使用智能指针

![image-20220413160756080](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131607201.png)

##### 正确使用new/delete

![image-20220413160911647](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131609769.png)

##### 合理处理new操作的错误

![image-20220413161052227](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131610339.png)

#### 错误处理

##### 使用恰当的错误处理机制

![image-20220413161143631](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131611743.png)

##### 避免在不宜抛异常的地方抛出异常

![image-20220413161417500](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131614644.png)

##### 异常的抛出与捕获

![image-20220413161703298](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131617426.png)

#### 模板与泛型编程

##### 使用模板时留意代码膨胀等负面效果

![image-20220413162013929](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131620061.png)

#### 并发与并行

##### 避免数据竞争

![image-20220413162136287](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131621404.png)

##### 注意对象的生命周期

![image-20220413162331180](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131623303.png)

##### 不要直接调用mutex的方法

![image-20220413162644214](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131626332.png)

##### 正确使用条件变量

![image-20220413162740068](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131627189.png)

#### 预处理

##### 预处理指令

![image-20220413162951290](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131629408.png)

##### 避免使用宏

![image-20220413163144912](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131631034.png)

#### 头文件和源文件

##### 头文件的包含方式

![image-20220413163706406](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131637538.png)



##### 头文件应该自包含

![image-20220413163854577](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131638698.png)

##### 禁止包含用不到的头文件

![image-20220413163947306](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131639501.png)

##### 使用命名空间管理源代码

![image-20220413164031036](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131640155.png)

#### 标准库

##### 优先使用C++标准库

![image-20220413164338629](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131643841.png)

##### 使用std::string表示字符串

![image-20220413170317464](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131703594.png)

##### 使用有效的迭代器

![image-20220413170512729](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131705847.png)

##### 正确使用格式化输入/输出函数

![image-20220413170800015](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131708148.png)

##### 防止命令注入问题

![image-20220413170839918](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131708039.png)

##### 线程不安全函数/异步不安全函数

![image-20220413171031907](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131710016.png)

### 继承自C语言编程规范条款

#### 宏封装

![image-20220413171220701](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131712824.png)

#### 变量

- 栈溢出

![image-20220413171352602](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131713722.png)

#### 控制语句

- 循环安全退出

![image-20220413171516798](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131715912.png)

#### 整数

- 枚举

![image-20220413171611091](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131716204.png)

#### 指针和数组

![image-20220413171654677](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131716790.png)

#### 断言

![image-20220413171747419](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131717525.png)

#### 函数使用

##### 禁止重定义安全函数

![image-20220413172013533](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131720650.png)

![image-20220413172030599](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131720710.png)

##### 正确设置destMax

![image-20220413172101130](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131721242.png)

#### 内存

![image-20220413172121862](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131721015.png)

#### 文件

![image-20220413172137486](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131721592.png)

#### 其他

![image-20220413172211301](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204131722419.png)



## 现代C++实战

### C++特点与演化

### 资源管理和对象的基本规则

### 移动语义和右值引用

### 智能指针

### 容器和类容器

### 迭代器和循环

### 对象返回和异常

### 语言易用性改进

### 部分其他新特性

### 错误处理

### 视图

### 模板基础

### 函数式编程

### 编译期计算

###  并发编程

### C++20与未来展望



## clean code

### clean code 定义

#### 代码简洁/可读的指导思想和实例

- 软件适应变化易于阅读的能力：易于阅读、易于实现、怡到好处

![image-20220412172722974](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121727108.png)

- 代码简洁案例：
  - 打印链表数据：
  - 查看天数：

![image-20220412172921948](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121729047.png)

![image-20220412172953765](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121729881.png)

#### 代码可维护的指导思想和实例

- 软件可修改、可扩展和可复用的能力：纠错、改进、新增需求或功能规格变化的适应能力

![image-20220412173056048](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121730190.png)

- 代码可维护案例：抽象基类接口。

![image-20220412173155892](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121731039.png)

#### 代码可测的指导思想和实例

- 软件发现故障并隔离、定位故障的能力，以及在一定的时间和成本前提下，进行测试设计、测
  试执行的能力：**可隔离、可控制、可观测、可定位**
- TDD是保证可测试性的较好手段

![image-20220412173314670](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121733802.png)

- 代码可测试案例：
  - 左侧：测试部件A，而部件A需要调用部件B，部件B又要调用部件C，我们要通过M把ABC解耦。
  - 右侧：参数过多，函数太大。

![image-20220412173631376](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121736801.png)

#### 代码可靠的指导思想和实例

- 代码可靠性是软件在给定时间间隔和环境条件下，按设计要求成功运行程序的概率：**预防、容错、自愈**

![image-20220412173753881](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121737018.png)

- 代码可靠性案例：

![image-20220412173844193](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121738305.png)

#### 代码高效的指导思想和实例

- 尽量少的使用系统资源的能力：CPU、内存、硬盘、网络带宽等

![image-20220412192938753](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121929873.png)

- 代码高效实例：
  - 减少冗余计算：直接判断首字符是否为空；
  - 减少无效计算：calculate()函数在调用时才计算；
  - 最快路由寻找：

![image-20220412193107961](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121931054.png)

![image-20220412193211840](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121932965.png)

![image-20220412193330894](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121933986.png)

#### 代码可移植的指导思想和实例

- 适应不同环境的能力：CPU、编译器、操作系统、软硬件平台与环境等

![image-20220412193543058](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121935169.png)

- 代码可移植实例：

  - 为了应对硬件、环境的变化，软件需要做可移植性设计，除了软件本身要做好抽象，避免直接使用硬件、环境相关的代码以外，也要**结合硬件归一化设计来共同提升可移植性**。因此，使用归一化的硬件和软件，好过考虑复杂的可移植性问题。公司绝大部分嵌入式系统都已经迁移到**ETOS和ARM CPU**上，大大缓解了对可移植性的要求。但是，不同的ARM或者SOC还是存在差异。

  - 不同的编译器，对数据类型的宽度定义不一样，比如同样是it类型，16/32编译器就存在差异。

    for (unsigned int i =0;i<=0xffff,++i)

  - 相同的接口，在不同的操作系统下的实现也会有差异，比如**sleep()**接口在windows和liux下参数单位就不同。

#### 代码安全的指导思想和实例

- 软件面对威胁的防护能力，并能保持系统信息的机密性、完整性和可用性：未授权访问/使用、泄露、破坏、篡改、毁灭等

![image-20220412193747331](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121937442.png)

- 代码安全实例：
  - 左：sizeof(指针)不正确，且需要把dst字符串长度传入
  - 右：输入-1，转换成全f,buf溢出

![image-20220412194908370](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204121949463.png)

#### Clean Code在不同维度的权衡

- 一般的优先级：安全》可靠》可读》可维护》可测试》高效》可移植
- 个别场景下，不同维度之间可能还会存在冲突“
  - 有些热点函数要进行**性能**优化，必然破坏**可读性**，但是此类函数不多，性能
    又必须达标，只能权衡牺牲少部分函数的可读性。
  - 安全编码所要求的**防御式编程**一定程度上影响**可读性、性能**，但是一旦产生
    安全问题，造成的后果很严重，所以经过权衡防御式编程的优先级更高。
  - 编码中适当使用多态、多线程、中断、异常、函数指针等可以提升代码的**可**
    **修改/可扩展性**，但过度使用会导致**可读性**严重下降，调试也非常困难。

### Clean Code评估

![image-20220412201619650](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204122016765.png)

##### WTF评估方法

![image-20220412202156480](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204122021599.png)

##### 工具化评估方法

![image-20220412202517699](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204122025818.png)

### Clean Code 达成

#### 数通Clean Code模型

![image-20220412202917093](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204122029237.png)

#### Clean Code最后屏障

![image-20220412203203243](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204122032468.png)

#### 华为Clean Code的达成是一个系统工程

![image-20220412203555104](https://gitee.com/wwwwaaa/web_img/raw/master/img/202204122035462.png)



# 
